import java.io.*;
import java.net.*;
import java.util.* ;
import java.lang.*;

public class Server {
	public static final int port = 2407;

	public static  void main(String[] args)throws IOException {
		new Server().runServer();
	}

	public void runServer() throws IOException{

		ServerSocket serverSocket = new ServerSocket(port);
		System.out.println("Server up & ready for conections...");
		while(true){
			Socket socket = serverSocket.accept();
			
		
			new ServerThread(socket).start();
		
		}

	}

	public class ServerThread extends Thread{
			Socket socket;
			ServerThread(Socket socket){

				this.socket = socket;
			}

		public void run() {
			try{
				String message = null;
				PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
				
				Scanner bufferedReader = new Scanner(socket.getInputStream());
				message = bufferedReader.nextLine();
				String[] directory = message.split(" ");
				int lengthfile = directory[1].length();
				String pathFile = (directory[1].equals("/"))? "hola.html": (directory[1].substring( 1, lengthfile));	
				File file = new File(pathFile);
				
				printWriter.println("HTTP/1.1 200 OK");
		 		printWriter.println("Connection close");
		 		printWriter.println("Date: Thu, 06 Aug 1998 12:00:15 GMT");
		 		printWriter.println("Server: miServidor");
		 		printWriter.println("Last-Modified: Mon, 22 Jun 1998 12:00:15 GMT");
		 		printWriter.println("Content-Length: " + file.length());
		 		printWriter.println("Content-Type: text/html");
		 		printWriter.println("\n");


				if (directory[0].equals("GET")) {
				BufferedReader pageStart = new BufferedReader(new InputStreamReader(new FileInputStream(file))); 
	            String pageNewLine = "";
	            String pageNewLine2 = "";
	            while((pageNewLine = pageStart.readLine()) != null){
	            	pageNewLine2 += pageNewLine + "\n";

	            }
	           		
					printWriter.print(pageNewLine2);
	            	System.out.println(pageNewLine2);

				
				}	     	 

				socket.close();
			} catch(IOException e){
				e.printStackTrace();
			
			}



		}


	//---------------
	}

//----fin
}