import java.io.*;
import java.util.Scanner;

public class Circle {
    private static double radius;


        public static void main(String[] args) throws IOException { 
               
                System.out.println("Enter the radius of the circle");
                Scanner in = new Scanner(System.in); 
                
                 Double a = in.nextDouble();
                 setRadius(a);
                  Double b = calculateDiameter() ;
                  Double c = calculateCircumference() ;
                  Double d =  calculateArea() ;
                  String car = ( "Diameter:     " + b + "\n" + "Circumference:      " + c + "\n" + "Area:       " + d + "\n");
                  String caracteristicas = "Circle Characteristics" + "\n"  + "Side:     " + a + "\n"  + car ;
                  System.out.println(caracteristicas);

        }

    public static double getRadius() {
	return radius;
    }

    public static void setRadius(double value)  {
        if( radius < 0 )
	    radius = 0.00;
	else
	    radius = value;
    }

    public static double calculateDiameter() {
        return radius * 2;
    }

    public  static double calculateCircumference() {
        return calculateDiameter() * 3.14159;
    }
	
    public static double calculateArea() {
        return radius * radius * 3.14159;
    }
}

