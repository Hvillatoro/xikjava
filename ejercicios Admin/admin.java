package functionalTests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import com.gargoylesoftware.htmlunit.javascript.host.XPathExpression;
import com.sun.corba.se.spi.orbutil.fsm.Action;
import com.thoughtworks.selenium.webdriven.commands.Open;

import org.testng.annotations.Test;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;


import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class admin extends Beforeclass{
	
	@Test
	public void d() {
		try {
			
		
			
		

		WebElement searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("buttonMenu")));
		searchField.click();
	
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class = 'dropdown-menu']//*[@class= 'ng-scope']//*[text()= 'traininghernan']")));
		searchField.click();
	
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='admin_button']")));
		searchField.click();
	
	
		adduser();
		ReadUrlFile.Wait(1000);
		slide();
		ReadUrlFile.Wait(1000);
		verify();
		ReadUrlFile.Wait(1000);
		ver();
		ReadUrlFile.Wait(7000);
		borrar();
		
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	public void slide(){
		try{
			WebElement searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='bt-sm-e']")));
			searchField.click();
		
		
	searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='user_params']//*[@oldtitle = 'trainingsa2@gmail.com']//..//*[@href = '#']/..//a")));
    WebElement slider = searchField;
    Actions move = new Actions(driver);
    int width=slider.getSize().getWidth();
    org.openqa.selenium.interactions.Action action  = move.dragAndDropBy(slider, (-width*100), 0).build();
    action.perform();

    searchField = wait.until(ExpectedConditions.presenceOfElementLocated( By.xpath("//*[@id='user_params']//*[@oldtitle = 'training1@gmail.com']//..//*[@href = '#']/..//a")));
    slider = searchField;
    width=slider.getSize().getWidth();
    action  = move.dragAndDropBy(slider, (-width*100), 0).build();
    action.perform();

   
    
    searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='user_params']//*[@oldtitle = 'training2@gmail.com']//..//*[@href = '#']/..//a")));
    slider = searchField;
    width=slider.getSize().getWidth();
    action  = move.dragAndDropBy(slider, (-width*100), 0).build();
    action.perform();
    
   
    
    searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='user_params']//*[@oldtitle = 'training3@gmail.com']//..//*[@href = '#']/..//a")));
    slider = searchField;
    width=slider.getSize().getWidth();
    action  = move.dragAndDropBy(slider, (-width*100), 0).build();
    action.perform();
    
   
    
    
    searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='user_params']//*[@oldtitle = 'training4@gmail.com']//..//*[@href = '#']/..//a")));
    slider = searchField;
    width=slider.getSize().getWidth();
    action  = move.dragAndDropBy(slider, (-width*100), 0).build();
    action.perform();
    
    ReadUrlFile.Wait(1000);
    searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='bt-sm-s']")));
	searchField.click();
	ReadUrlFile.Wait(1000);
	searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class = 'ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable']//*[@class = 'ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//*[@class = 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//*[text() = 'Ok']")));
	searchField.click();
    
    }catch(Exception p){
		
		Assert.fail("no se pudo mover los slide");
	}
    
    }
	public void adduser(){
		try{
		String nombret ;
		String correot;
		int i=1;
		
		while( i != 5){
			nombret = "test" + i ;
			correot = "training"+ i+"@gmail.com";
			ReadUrlFile.Wait(1000);
		WebElement searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='bt-invite-user']")));
		searchField.click();
		
		
		
		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("firstName")));
		searchField.sendKeys(nombret);
		

		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='lastName']")));
		searchField.sendKeys("training");
		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='email']")));
		searchField.sendKeys(correot);
		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='quota']")));
		searchField.sendKeys("10");
		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='cellphone']")));
		searchField.sendKeys("12345678");
		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='company']")));
		searchField.sendKeys("test");
		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='ui-information']/..//*[@class = 'ui-dialog-buttonset']//*[text() = 'Save']")));
		searchField.click();
		ReadUrlFile.Wait(1000);
		i++;
		}
		
		}catch(Exception u){
		
			Assert.fail("no se pudo agregar usuarios");
		}
	}
	
	public void verify(){
		WebElement searchField ;
		try{
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='chart7']//*[text() = '49%']")));
			String ver = searchField.getText();
			System.out.println(ver +"Space Avaliable to Assign");
			
			
					
		}catch(Exception o) {
			Assert.fail("no esta bien la grafica");
			
			
		}
		
		
		
		
	}
	public void borrar(){
		int i = 0;
		WebElement searchField;
				while(i != 4){
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='bt-sm-e']")));
		searchField.click();
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='user_params']//*[@class = 'col_account'][1]/button[2]")));
		searchField.click();
		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id = 'Confirm-del-user']/..//*[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[1]")));
		searchField.click();
			i++;
				}
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='bt-sm-e']")));
		searchField.click();		
		ReadUrlFile.Wait(1000);		
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='user_params']//*[@oldtitle = 'trainingsa2@gmail.com']//..//*[@href = '#']/..//a")));
	    WebElement slider = searchField;
	    Actions move = new Actions(driver);
	    org.openqa.selenium.interactions.Action action  = move.dragAndDropBy(slider, 30, 0).build();
	    action.perform();
	    
	    ReadUrlFile.Wait(1000);
	    searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='bt-sm-s']")));
		searchField.click();
		ReadUrlFile.Wait(1000);
		searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class = 'ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable']//*[@class = 'ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//*[@class = 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//*[text() = 'Ok']")));
		searchField.click();
		ReadUrlFile.Wait(3000);	
	}
	public void ver(){
		WebElement searchField ;
		String cuadro;
		String nombret ;
		try{
			
			
			
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='chart7']//tbody/tr/td[contains(text(),'Workspace')]")));
			cuadro = searchField.getText();
			
			System.out.println("espacio "+ cuadro + " existente");
		
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='chart7']//tbody/tr/td[contains(text(),'Pr�nom Nom de famille')]")));
			cuadro = searchField.getText();
			
			System.out.println("espacio "+ cuadro + " existente");
			
			
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='chart7']//tbody/tr/td[contains(text(),'test1 training')]")));
			cuadro = searchField.getText();
			
			System.out.println("espacio "+ cuadro + " existente");
			
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='chart7']//tbody/tr/td[contains(text(),'test2 training')]")));
			cuadro = searchField.getText();
			
			System.out.println("espacio "+ cuadro + " existente");

			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='chart7']//tbody/tr/td[contains(text(),'test3 training')]")));
			cuadro = searchField.getText();
			
			System.out.println("espacio "+ cuadro + " existente");
			
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='chart7']//tbody/tr/td[contains(text(),'test4 training')]")));
			cuadro = searchField.getText();
			
			System.out.println("espacio "+ cuadro + " existente");



			
		}catch(Exception g) {
				Assert.fail("no encuentra usuarios");
				
				
			}
	}
	
	private int name(String string) {
		// TODO Auto-generated method stub
		return 0;
	}
}