package visualTests;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.ReadUrlFile;

/**xik
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio5 extends beforeClass{
	
	
	
	@Test
	public void f() {
		try {
			

ReadUrlFile.Wait(5000);
			
			Region folder1 ;
			Region folder2 ;
			
			Region region = screen.wait("img/ejercicio2/newf.jpg");
			region.click();

			ReadUrlFile.Wait(2000);
			region.paste("D1");
		
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/ejercicio2/newd.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/ejercicio2/newf.jpg");
			region.click();

			ReadUrlFile.Wait(2000);
			region.paste("D2");
		
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/ejercicio2/newd.jpg");
			region.click();
			//-------
			ReadUrlFile.Wait(2000);
			folder1 = screen.wait("img/ejercicio5/d1arch.jpg");
			
			ReadUrlFile.Wait(2000);
			folder2 = screen.wait("img/ejercicio5/d2.jpg");

			ReadUrlFile.Wait(2000);
			Region t1 = folder1.wait("img/ejercicio5/carpeta.jpg");
			
			ReadUrlFile.Wait(2000);
			Region t2 = folder2.wait("img/ejercicio5/carpeta.jpg");
			
			ReadUrlFile.Wait(2000);
			screen.dragDrop(t2,t1);
			ReadUrlFile.Wait(4000);
			
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/ejercicio5/d1fold.jpg");
			region.click();
			
			ReadUrlFile.Wait(4000);
			region = screen.wait("img/ejercicio2/home.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			folder1.wait("img/ejercicio2/d1.jpg");
			
			ReadUrlFile.Wait(2000);
			region = folder1.wait("img/ejercicio2/bullet.jpg");
			folder1.click();
		
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/delete.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/dbutton.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/trash.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/bullet.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/delete.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/dbutton.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/home.jpg");
			region.click();
			
			
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
