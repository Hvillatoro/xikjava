package visualTests;

import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**xik
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio1 extends Browsers{
	// Define screen for sikuli.
	Screen screen;
	
	// Initialize 
	@BeforeClass
	public void init() {
		screen = new Screen();
		Settings.MinSimilarity = 0.70;
		Settings.Highlight = true;
	}
	
	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");

			ReadUrlFile.Wait(5000);
			
			Region region = screen.wait("img/ejercicio1/user.jpg");
			region.click();
			

			region.paste("testx3@gmail.com");
		
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio1/password.jpg");
			region.click();
		

			region.type("Ab1234");
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio1/log.jpg");
			region.click();
			
		
			
			ReadUrlFile.Wait(4000);
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
