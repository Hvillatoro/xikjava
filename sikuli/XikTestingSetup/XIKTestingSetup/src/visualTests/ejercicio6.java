package visualTests;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.ReadUrlFile;

/**xik
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio6 extends beforeClass{
	
	
	
	@Test
	public void f() {
		try {
			

			ReadUrlFile.Wait(5000);
			
			Region folder ; 
			ReadUrlFile.Wait(2000);
			Region region = screen.wait("img/ejercicio2/newf.jpg");
			region.click();
			

			
		
			ReadUrlFile.Wait(2000);
			region.paste("COM1");
		
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/ejercicio2/newd.jpg");
			region.click();
			
			ReadUrlFile.Wait(500);
			folder = screen.wait("img/ejercicio6/invalid.png");
			
			
			
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
