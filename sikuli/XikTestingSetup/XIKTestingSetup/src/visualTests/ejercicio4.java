package visualTests;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.ReadUrlFile;

/**xik
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio4 extends beforeClass{
	
	
	
	@Test
	public void f() {
		try {
			

			ReadUrlFile.Wait(5000);
			
			Region folder ; 
			
			Region region = screen.wait("img/ejercicio4/add.jpg");
			region.click();
			

		
			ReadUrlFile.Wait(4000);
			region = screen.wait("img/ejercicio4/addfile.jpg");
			region.click();
			
			ReadUrlFile.Wait(4000);
			 region = screen.wait("img/ejercicio4/prueba.jpg");
			 region.doubleClick();
			
			 
			ReadUrlFile.Wait(3000);
			 region = screen.wait("img/ejercicio4/subir.jpg");
			 region.click();
		
			ReadUrlFile.Wait(10000);
			 region = screen.wait("img/ejercicio4/goback.jpg");
			 region.click();
			
			 
			ReadUrlFile.Wait(4000);
			folder = screen.wait("img/ejercicio4/archfile.jpg");
			
			ReadUrlFile.Wait(3000);
			 region = folder.wait("img/ejercicio2/bullet.jpg");
			 region.click();
			
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/delete.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/dbutton.jpg");
			region.click();

			
			ReadUrlFile.Wait(4000);
			region = screen.wait("img/ejercicio2/trash.jpg");
			region.click();
			
			ReadUrlFile.Wait(3000);
			 region = screen.wait("img/ejercicio2/bullet.jpg");
			 region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/delete.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/dbutton.jpg");
			region.click();
			
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/home.jpg");
			region.click();
		
			
			ReadUrlFile.Wait(2000);
			
			
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
