package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio5 extends Browsers{

	@Test
	public void f() {
		
		try {
			driver.get("http://104.131.93.237/file-storage");
		driver.findElement(By.id("email")).sendKeys("testx3@gmail.com");
		driver.findElement(By.id("password")).sendKeys("Ab1234");
			
		
		
		ReadUrlFile.Wait(1000);
		driver.findElement(By.id("bt_send")).click();
		ReadUrlFile.Wait(10000);
		
       
		    
            Robot robot = new Robot();

            int mask = InputEvent.BUTTON1_MASK;
    		robot.mouseMove(900, 350);           
    		robot.mousePress(mask); 
    		 robot.delay(4000);
    		robot.mouseMove(570, 350);  
    		robot.delay(5000);
    		robot.mouseRelease(mask);
             robot.delay(3000);
             robot.mousePress(mask); 
             robot.mouseRelease(mask);
           
            
    	//	 driver.findElement(By.xpath("/html/body/div[1]/div[7]/div[3]/div[7]/div[2]/div[2]")).click(); 
     	//	ReadUrlFile.Wait(1000);
		
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}