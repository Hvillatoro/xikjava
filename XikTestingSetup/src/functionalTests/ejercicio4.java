package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio4 extends Browsers{

	@Test
	public void f() {
		
		try {
			driver.get("http://104.131.93.237/file-storage");
		driver.findElement(By.id("email")).sendKeys("testx3@gmail.com");
		driver.findElement(By.id("password")).sendKeys("Ab1234");
			
		
		
		ReadUrlFile.Wait(1000);
		driver.findElement(By.id("bt_send")).click();
		ReadUrlFile.Wait(1000);
		
       
		
		driver.findElement(By.xpath("//*[@id='add']")).click(); 
		ReadUrlFile.Wait(1000);
		
		
		driver.findElement(By.xpath("//*[@id='fileupload']//*[@class='ui-button-text']//*[text()='Add files']")).click(); 
		ReadUrlFile.Wait(1000);
		
		
		
            
            Robot robot = new Robot();
            // Creates the delay of 5 sec so that you can open notepad before
            // Robot start writting
            robot.delay(2000);
            robot.keyPress(KeyEvent.VK_P);
            robot.keyPress(KeyEvent.VK_R);
            robot.keyPress(KeyEvent.VK_U);
            robot.keyPress(KeyEvent.VK_E);
            robot.keyPress(KeyEvent.VK_B);
            robot.keyPress(KeyEvent.VK_A);
            robot.keyPress(KeyEvent.VK_PERIOD);
            robot.keyPress(KeyEvent.VK_T);
            robot.keyPress(KeyEvent.VK_X);
            robot.keyPress(KeyEvent.VK_T);
            robot.delay(2000);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.delay(3000);
            driver.findElement(By.xpath("//button[contains(., 'Start')]")).click(); 
    		ReadUrlFile.Wait(1000);
    		 driver.findElement(By.xpath("//button[contains(., 'Go Back')]")).click(); 
     		ReadUrlFile.Wait(1000);
		
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}