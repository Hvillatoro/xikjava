import java.io.*;
import java.util.Scanner;

public class exercise3 {
    public static void main(String[] args) throws IOException { 
       	double n1 ;
		double n2 ;


		System.out.println("Welcome to Calculator" + "\n" +  "\n");

		System.out.println("Enter the first number");
		Scanner in = new Scanner(System.in); 
		n1 = in.nextDouble();
		System.out.println("Enter the second number");
		n2 = in.nextDouble();
		String sum = suma(n1,n2);
		String res = resta(n1,n2);
		String mul = multiplicacion(n1,n2);
		String div = division(n1,n2);
		System.out.println("\n");
		String fin = new String("resultado" + "\n" + "\n" + "Add"+ "\n"  + sum  + "\n" + "subtract"+ "\n"  +  res + "\n" + "multiply"+ "\n"  + mul + "\n" + "divide "+ "\n"+ div + "\n");
		System.out.println(fin);

	}


	public static String suma(double num1, double num2){
		String suma = new String();
		double resultado ;

		resultado = num1 + num2;

		suma = num1 + "  +  " + num2 + "  =  "+ resultado;
		return suma;
	}


	public static  String resta(double num1, double num2){
		String resta = new String();
		double resultado ;

		resultado = num1 - num2;

		resta = num1 + "  -  " + num2 + "  =  "+ resultado;
		return resta;
	}

	public static  String multiplicacion(double num1, double num2){
		String multiplicacion = new String();
		double resultado ;

		resultado = num1 * num2;

		multiplicacion = num1 + "  *  " + num2 + "  =  "+ resultado;
		return multiplicacion;
	}

	public static  String division(double num1, double num2){
		String division = new String();
		double resultado ;

		resultado = num1 / num2;

		division = num1 + "  /  " + num2 + "  =  "+ resultado;
		return division;
	}
}