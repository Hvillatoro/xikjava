package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;


import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio3 extends Browsers{

	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
		driver.findElement(By.id("email")).sendKeys("testx3@gmail.com");
		driver.findElement(By.id("password")).sendKeys("Ab1234");
			
		
		
		ReadUrlFile.Wait(1000);
		driver.findElement(By.id("bt_send")).click();
		ReadUrlFile.Wait(1000);
		
       
		
		driver.findElement(By.xpath("//parent::div[@class = 'filename' and text() = 'Training']/../div[3]")).click(); 
		ReadUrlFile.Wait(1000);
		
	
		driver.findElement(By.xpath("//*[@id='context_move']")).click(); 
		ReadUrlFile.Wait(1000);
		
		
		driver.findElement(By.xpath("//*[@id='move-form']//*[@id = 'tf_0']//*[@class = 'jstree-last jstree-leaf']//*[@href = '#']")).click(); 
		ReadUrlFile.Wait(1000);
		
		driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/button[1]")).click(); 
		ReadUrlFile.Wait(1000);
		
		
		driver.findElement(By.xpath("//parent::div[@class = 'filename' and text() = 'trashbin']/../div[2]")).click(); 
		ReadUrlFile.Wait(1000);
		
		
		
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}