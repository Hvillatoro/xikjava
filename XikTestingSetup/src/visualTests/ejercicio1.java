package visualTests;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio1 extends Browsers{
	// Define screen for sikuli.
	Screen screen;
	
	// Initialize 
	@BeforeClass
	public void init() {
		screen = new Screen();
		Settings.MinSimilarity = 0.7;
	}
	
	@Test
	public void f() {
		try {
			driver.get("http://www.google.com");
			
			 Robot robot = new Robot();
	            
	            robot.delay(2000);
	            robot.keyPress(KeyEvent.VK_X);
	            robot.keyPress(KeyEvent.VK_I);
	            robot.keyPress(KeyEvent.VK_K);
	            robot.keyPress(KeyEvent.VK_PERIOD);
	            robot.keyPress(KeyEvent.VK_G);
	            robot.keyPress(KeyEvent.VK_T);
	            robot.delay(2000);
	            Screen s = new Screen();
	           
	                    s.click("searchgoogle.png", 0);
	                   

	            
			
			
		
			ReadUrlFile.Wait(4000);
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
