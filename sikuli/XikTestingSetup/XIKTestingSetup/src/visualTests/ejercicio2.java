package visualTests;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.ReadUrlFile;

/**xik
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class ejercicio2 extends beforeClass{
	
	
	
	@Test
	public void f() {
		try {
			

			ReadUrlFile.Wait(5000);
			
			Region folder ; 
			
			Region region = screen.wait("img/ejercicio2/newf.jpg");
			region.click();
			

			
		
			ReadUrlFile.Wait(2000);
			region.paste("New Directory");
		
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/newd.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 folder = screen.wait("img/ejercicio2/newdirectory.jpg");
			
			 ReadUrlFile.Wait(2000);
			 region = folder.wait("img/ejercicio2/carpeta.jpg");
			region.click();
			
			ReadUrlFile.Wait(6000);
			region = screen.wait("img/ejercicio2/home.jpg");
			region.click();
			
			 
			ReadUrlFile.Wait(2000);
			 region = folder.wait("img/ejercicio2/bullet.jpg");
			folder.click();
		
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/delete.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/dbutton.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/trash.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/bullet.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/delete.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/dbutton.jpg");
			region.click();
			
			ReadUrlFile.Wait(2000);
			 region = screen.wait("img/ejercicio2/home.jpg");
			region.click();
			
			
			ReadUrlFile.Wait(2000);
			
			
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
