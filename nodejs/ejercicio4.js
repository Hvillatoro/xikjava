// Constructor
function Humano(nameh, age, colorh) {
  // always initialize all instance properties
  this.nameh = nameh;
  this.age = age; // default value
  this.colorh = colorh;


}
// class methods
Humano.prototype.getName = function() {
	console.log(this.nameh);
};
Humano.prototype.getAge = function() {
	console.log(this.age);
};
Humano.prototype.getColor = function() {
	console.log(this.colorh);
};

var human = new Humano('miguel', 20, 'azul');

human.getName();
human.getAge();
human.getColor();