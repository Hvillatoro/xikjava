package functionalTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Beforeclass extends Browsers{
	
	@BeforeClass
	public void init() {
		try {
			
			
			driver.get("http://104.131.93.237/file-storage");
			
			WebElement searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
			searchField.sendKeys("trainingsa2@gmail.com");
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
			searchField.sendKeys("Ab1234");
			
			
			searchField = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("bt_send")));
			searchField.click();
			
			
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}




}